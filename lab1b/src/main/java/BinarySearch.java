import java.util.List;

public class BinarySearch {

    public void binarySearch (List<Integer> list, int target){
        int fIndex = 0 ;
        int lIndex = list.size()-1;

        int mid = (fIndex + lIndex)/2;

        while (fIndex <= lIndex){
            if (list.get(mid)<target){
                fIndex = mid + 1;
            }
            else if (list.get(mid) == target){
                System.out.println("Target "+target+" number is present at index "+(mid)+".");
                break;
            }
            else {
                lIndex = mid - 1;
            }
            mid = (fIndex+lIndex)/2;
        }
        if (fIndex > lIndex)
            System.out.println("Target "+target+"  number is not present in array.");
    }

}
