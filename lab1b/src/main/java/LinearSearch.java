import java.util.List;

public class LinearSearch {

    /**
     * The method linearSearch takes as parameters
     * @param list which provides a sorted list of intergers and
     * @param target which is the target number to find a pair and print its index and
     * @return which provides a value to check whether the target number is present or not
     */

    public int linearSearch(List<Integer> list , int target) {
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i) == target) {
                return i;
            }
        }return -1;
    }
}
