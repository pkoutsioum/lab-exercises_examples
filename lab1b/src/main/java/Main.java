import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        List<Integer> list = Arrays.asList(2, 3, 5, 7, 9);

//        System.out.println("Please give an integer number for sum to find a pair:");
//        Scanner in = new Scanner(System.in);
//        int target = in.nextInt();
        int target = 9;

        System.out.println("Choose\n\r1:Linear Search\n\r2:Binary Search");
        Scanner in = new Scanner(System.in);
        int choise = in.nextInt();

        switch (choise) {
            case 1:
                LinearSearch targetValueAtArrayLS = new LinearSearch();
                int res = targetValueAtArrayLS.linearSearch(list, target);

                if (!(res == -1)) {

                    System.out.print("Target " + target + " number is present at index " + res + ".");
                } else {
                    System.out.print("Target " + target + "  number is not present in array.");
                }
                break;
            case 2:
                BinarySearch targetValueAtArrayBS = new BinarySearch();
                targetValueAtArrayBS.binarySearch(list, target);
                break;
            default:
                System.out.println("Not pressed 1 or 2");
                break;
        }
    }
}
