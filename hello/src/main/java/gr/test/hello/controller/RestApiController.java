package gr.test.hello.controller;

import gr.test.hello.model.Employee;
import gr.test.hello.service.EmployeeService;
import gr.test.hello.util.EmployeeError;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
//import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@RestController
@Slf4j
//@RequestMapping(value = "/api/")

public class RestApiController {

    @Autowired
    EmployeeService employeeService;

    //@RequestMapping(value = "/testme", method = RequestMethod.GET)
    @GetMapping(value = "/testme")
	public ResponseEntity<String> testme() {
        log.debug("Entering {}","testme");
        String myreturn="HELLO WORLD FROM SPRING";
        log.debug("Leaving {}", "testme");
        return new ResponseEntity<String>(myreturn, HttpStatus.OK);
	}

	@GetMapping(value = "/employee")
    public ResponseEntity<List<Employee>> listAllUsers(){
        List<Employee> employee = employeeService.findAllEmployees();
        if (employee.isEmpty()){
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<List<Employee>>(employee, HttpStatus.OK);
    }

    @RequestMapping(value = "/employee/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getUser(@PathVariable("id") long id) {
        log.info("Fetching Employee with id {}", id);

        Employee employee = employeeService.findById(id);
        if (employee == null) {
            log.error("Employee with id {} not found.", id);
            return new ResponseEntity(
                    new EmployeeError("Employee with id " + id + " not found."),
                    HttpStatus.NOT_FOUND);
        }
        employeeService.findById(id);
        return new ResponseEntity<Employee>(employee,HttpStatus.OK);
    }

    @RequestMapping(value = "/employee/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteEmployee(@PathVariable("id") long id) {
        log.info("Fetching & Deleting Employee with id {}", id);

        Employee employee = employeeService.findById(id);
        if (employee == null) {
            log.error("Unable to delete. Employee with id {} not found.", id);
            return new ResponseEntity(
                    new EmployeeError("Unable to delete. Employee with id " + id + " not found."),
                    HttpStatus.NOT_FOUND);
        }
        employeeService.deleteEmployeeById(id);
        return new ResponseEntity<Employee>(HttpStatus.NO_CONTENT);
    }
}
