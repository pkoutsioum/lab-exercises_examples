package gr.test.hello;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication (scanBasePackages=("gr.test.hello.controller, gr.test.hello.service"))
//@RestController
@Configuration
@PropertySource("application.properties")

public class HelloWorldApplication {
	public static void main(String[] args) {
		SpringApplication.run(HelloWorldApplication.class, args);
	}
//	@RequestMapping("/")
//	public String sayHello() {
//		return "Hello World!";
//	}
}