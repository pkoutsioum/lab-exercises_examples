import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {

        List<String> originalList = Arrays.asList("abc", "", "bc", "efg", "drdvs","", "182");
        List<String> filteredList = originalList.stream().filter(string -> !string.isEmpty()).collect(Collectors.toList());

        System.out.println("Filtered list is: "+filteredList);

    }
}
