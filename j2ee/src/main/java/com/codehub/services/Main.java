package com.codehub.services;

import com.codehub.model.Item;

import java.io.IOException;
import java.util.List;

public class Main {
    public static void main(String[] args) throws Exception {

        List<Item> list = StaxParser.readConfig("test.xml");
        System.out.println(list);

        StaxParser.saveConfig(list, "test2.xml");

        JsonIO jsonIO = new JsonIOImpl();
        jsonIO.saveToFileAsJSON(list, "items.json");

    }
}
