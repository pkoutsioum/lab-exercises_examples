package com.codehub.services;

import com.codehub.model.Item;

import java.util.List;

public interface JsonIO {
    void saveToFileAsJSON(List<Item> items, String filename);
    List<Item> readJSON(String filename);

}
